use aspnetdb
IF NOT EXISTS (select * from sys.objects where name = 'NoteUsers')
BEGIN
	create table NoteUsers (Id uniqueidentifier primary key, Email nvarchar(1024))
END

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'NoteUsers' AND COLUMN_NAME = 'TokenId')
     ALTER TABLE [NoteUsers] ADD [TokenId] uniqueidentifier NOT NULL;

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'NoteUsers' AND COLUMN_NAME = 'CreateDate')
     ALTER TABLE [NoteUsers] ADD [CreateDate] datetime NOT NULL;

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'NoteUsers' AND COLUMN_NAME = 'IsActive')
     ALTER TABLE [NoteUsers] ADD [IsActive] bit NOT NULL;
     
IF NOT EXISTS (select * from sys.objects where name = 'Feedbacks')
BEGIN
	create table Feedbacks (Id uniqueidentifier primary key,
	 Email nvarchar(1024) null,
	 Title nvarchar(1024) null,
	 Text nvarchar(MAX) null,
	 UserId uniqueidentifier null,
	 CreateDate datetime not null,
	 )
END

IF NOT EXISTS (select * from sys.objects where name = 'MailMessages')
BEGIN
	create table MailMessages (Id uniqueidentifier primary key,
	 Email nvarchar(1024) not null,
	 Message nvarchar(1024) not null,
	 Subject nvarchar(1024) not null,
	 CreateDate datetime not null,
	 )
END

IF NOT EXISTS (select * from sys.objects where name = 'UserTokens')
BEGIN
	create table UserTokens (Id uniqueidentifier primary key,	 
	 UserId uniqueidentifier not null,
	 CreateDate datetime not null,
	 )
END

------------------------------------------
IF NOT EXISTS (select * from sys.objects where name = 'KeyValues')
BEGIN
	create table KeyValues (Id uniqueidentifier primary key,[Key] nvarchar(MAX), Value nvarchar(MAX))
END
------------------------------------------

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ImportResults')
 CREATE TABLE [dbo].[ImportResults](
     [Id] [uniqueidentifier] NOT NULL,
     [SessionId] [uniqueidentifier] NOT NULL,
     [UserId] [uniqueidentifier] NOT NULL,
     [Status] [int] NOT NULL,
     [Date] [datetime] NOT NULL,
     [Title] [nvarchar] (4000) NOT NULL,
     [Artist] [nvarchar] (MAX) NOT NULL,
     [NormalizedTitle] [nvarchar] (4000) NOT NULL,
     [NormalizedArtist] [nvarchar] (MAX) NOT NULL,
     [CandidateTitle] [nvarchar] (4000) NOT NULL,
     [CandidateArtist] [nvarchar] (MAX) NOT NULL,
     [Message] [nvarchar] (MAX) NOT NULL,
     CONSTRAINT[PK_ImportResults] PRIMARY KEY CLUSTERED([Id] ASC)
 )