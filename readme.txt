%windir%\Windows\Microsoft.NET\Framework64\v2.0.50727\aspnet_regiis.exe -i
%windir%\Windows\Microsoft.NET\Framework64\v4.0.30319\aspnet_regiis.exe -i

execute the aspnet_regsql utility (in your C:\Windows\Microsoft.NET\Framework\v4.0.30319 directory - adapt to your framework version, and possibly use the Framework64 if you're running on a 64-bit machine) against your SU\SQLSERVER instance to create the ASP.NET membership database on your server

public class NinjectControllerFactory : DefaultControllerFactory
{
  private IKernel ninjectKernel;
  public NinjectControllerFactory()
  {
    ninjectKernel = new StandardKernel();
    AddBindings();
  }
 
  protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
  {
    return (controllerType == null) ? null : (IController)ninjectKernel.Get(controllerType);
  }
 
  private void AddBindings()
  {
    // put additional bindings here
    // ninjectKernel.Bind<...>().To<...>();
  }
}

protected void Application_Start()
{
  ...
  ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory());
}


private void AddBindings()
{
  // put additional bindings here
  ninjectKernel.Bind<IProductAccessor>().To<DBProductAccessor>();
}