﻿namespace Tolltech.ThisCore
{
    public static class ConfigKeys
    {
        public const string ServiceSectionNameKey = "serviceSection";
        public const string DbConnectionStringKey = "dbConnectionString";
        public const string FriendsDbConnectionStringKey = "friendsdbConnectionString";
        public const string UIDbConnectionStringKey = "uiDbConnectionString";
        public const string ListeningEndPointKey = "listeningEndPoint";
    }
}