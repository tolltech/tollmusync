namespace Tolltech.ThisCore
{
    public interface IConfiguration
    {
        string Get(string key);
    }
}