﻿using Tolltech.ThisCore.Sql;

namespace Tolltech.ThisCore
{
    public class ConnectionString : IConnectionString
    {
        private readonly string connectionString;

        public ConnectionString(IConfiguration configuration, string customDbConnectionStringKey = null)
        {
            this.connectionString = configuration.Get(string.IsNullOrWhiteSpace(customDbConnectionStringKey) ? ConfigKeys.DbConnectionStringKey : customDbConnectionStringKey);
        }

        public string Value
        {
            get { return connectionString; }
            private set { }
        }
    }
}