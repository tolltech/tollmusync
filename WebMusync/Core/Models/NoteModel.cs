﻿using System;

namespace Tolltech.ThisCore.Models
{
    public class NoteModel
    {
        public Guid Id { get; set; }

        public string Text { get; set; }

        public string Title { get; set; }

        public string[] HashTags { get; set; }

        public Guid UserId { get; set; }

        public DateTime TimeStamp { get; set; }

        public DateTime CreateDate { get; set; }

        public bool Deleted { get; set; }
    }
}