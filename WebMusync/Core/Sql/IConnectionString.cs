﻿namespace Tolltech.ThisCore.Sql
{
    public interface IConnectionString
    {
        string Value { get; }
    }
}