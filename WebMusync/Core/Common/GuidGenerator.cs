﻿using System;

namespace Tolltech.ThisCore.Common
{
    public class GuidGenerator : IGuidGenerator
    {
        public Guid Create()
        {
            return Guid.NewGuid();
        }
    }
}