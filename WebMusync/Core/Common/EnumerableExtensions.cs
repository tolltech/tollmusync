﻿using System.Linq;

namespace Tolltech.ThisCore.Common
{
    public static class EnumerableExtensions
    {
        public static IQueryable<T> TakePage<T>(this IQueryable<T> query, int skipCount, int pageSize)
        {
            return query.Skip(skipCount).Take(pageSize);
        }
    }
}