﻿using System;

namespace Tolltech.ThisCore.Common
{
    public class DateTimeService : IDateTimeService
    {
        public DateTime Now
        {
            get { return DateTime.Now; }
        }

        public long Ticks
        {
            get { return DateTime.Now.Ticks; }
        }

        public DateTime UtcNow
        {
            get { return DateTime.UtcNow; }
        }

        public long UtcTicks
        {
            get { return DateTime.UtcNow.Ticks; }
        }
    }
}