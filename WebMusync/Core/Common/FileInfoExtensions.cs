﻿using System.IO;

namespace Tolltech.ThisCore.Common
{
    public  static class FileInfoExtensions
    {
         public static string GetNameWithoutExtension(this FileInfo fileInfo)
         {
             var name = fileInfo.Name;
             var lastIndexOf = name.LastIndexOf(".");
             return lastIndexOf < 0 ? name : name.Remove(lastIndexOf);
         }
    }
}