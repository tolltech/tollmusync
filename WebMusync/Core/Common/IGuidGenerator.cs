﻿using System;

namespace Tolltech.ThisCore.Common
{
    public interface IGuidGenerator
    {
        Guid Create();
    }
}