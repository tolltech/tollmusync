﻿using System;

namespace Tolltech.ThisCore.Common
{
    public interface IDateTimeService
    {
        DateTime Now { get; }
        long Ticks { get; }

        DateTime UtcNow { get; }
        long UtcTicks { get; }
    }
}