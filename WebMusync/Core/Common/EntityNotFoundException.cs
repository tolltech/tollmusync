﻿using System;

namespace Tolltech.ThisCore.Common
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(string message)
            : base(message)
        {

        }
    }
}