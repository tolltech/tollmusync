﻿namespace Tolltech.ThisCore.Common
{
    public enum Sorting
    {
        Unknown = 0,
        Create = 1,
        Update = 2
    }
}