﻿using System.ComponentModel;
using System.Net;
using System.Net.Mail;
using log4net;

namespace Tolltech.ThisCore.Mail
{
    public class MailClient : IMailClient
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(MailClient));

        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            var token = (string) e.UserState;

            if (e.Cancelled)
            {
                log.ErrorFormat("[{0}] Send canceled.", token);
            }

            if (e.Error != null)
            {
                log.ErrorFormat("[{0}] {1}", token, e.Error);
            }
            else
            {
                log.Info("MessageSent");
            }
        }
        
        public void Send(string toEmail, string message, string subject)
        {
            var fromAddress = "noreply.musync.ru@mail.ru";
            var from = new MailAddress(fromAddress, "Musync.ru", System.Text.Encoding.UTF8);
            var to = new MailAddress(toEmail);
            using (var client = new SmtpClient())
            using (var mailMessage = new MailMessage())
            {
                client.Host = "smtp.mail.ru";
                client.Port = 587;                
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Credentials = new NetworkCredential(fromAddress, "tolltech123");

                mailMessage.From = from;
                mailMessage.To.Add(to);
                mailMessage.Body = message;
                mailMessage.Subject = subject;
                mailMessage.SubjectEncoding = System.Text.Encoding.UTF8;

                client.SendCompleted += SendCompletedCallback;
                client.Send(mailMessage);
            }
        }
    }
}