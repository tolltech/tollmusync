﻿namespace Tolltech.ThisCore.Mail
{
    public interface IMailClient
    {
        void Send(string toEmail, string message, string subject);
    }
}