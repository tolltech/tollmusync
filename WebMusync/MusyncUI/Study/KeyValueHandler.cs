﻿using System;
using System.Linq;
using Tolltech.ThisCore.Sql;

namespace Tolltech.MusyncUI.Study
{
    public class KeyValueHandler : IKeyValueHandler
    {
        private readonly IDataContext dataContext;

        public KeyValueHandler(IDataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public KeyValue Find(string key)
        {
            return dataContext.GetTable<KeyValue>().FirstOrDefault(x => x.Key == key);
        }

        public void Create(params KeyValue[] keyValues)
        {
            foreach (var keyValue in keyValues.Where(x => x.Id == Guid.Empty))
            {
                keyValue.Id = Guid.NewGuid();
            }
            dataContext.CreateAll(keyValues);
        }

        public KeyValue[] Select(string[] keys)
        {
            return dataContext.GetTable<KeyValue>().Where(x => keys.Contains(x.Key)).ToArray();
        }

        public void Update(params object[] existed)
        {
            dataContext.Update();
        }
    }
}