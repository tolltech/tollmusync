using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using log4net;
using Tolltech.ThisCore;
using Tolltech.ThisCore.Sql;

namespace Tolltech.MusyncUI.Study
{
    //[MimeType("application/json")] Json,
    //[MimeType("application/x-protobuf")] Protobuf,
    //[MimeType("application/x-grobuf")] Grobuf,
    //[MimeType("application/xml")] Xml,
    //"Content-Type"

    [AllowAnonymous]
    public class StudyController : Controller
    {
        private readonly IJsonSerializer jsonSerializer;
        private readonly IXmlSerialiazer xmlSerialiazer;
        private readonly IQueryExecutorFactory queryExecutorFactory;
        private const string xmlContentType = "application/xml";
        private const string jsonContentType = "application/json";
        private static readonly ILog log = LogManager.GetLogger(typeof(StudyController));

        public StudyController(IJsonSerializer jsonSerializer, IXmlSerialiazer xmlSerialiazer,
            IQueryExecutorFactory queryExecutorFactory)
        {
            this.jsonSerializer = jsonSerializer;
            this.xmlSerialiazer = xmlSerialiazer;
            this.queryExecutorFactory = queryExecutorFactory;
        }

        public class Input
        {
            public int K { get; set; }
            public decimal[] Sums { get; set; }
            public int[] Muls { get; set; }
        }

        public class Output
        {
            public decimal SumResult { get; set; }
            public int MulResult { get; set; }
            public decimal[] SortedInputs { get; set; }
        }

        public void SetCookie(string domain = "musync.ru", string name = "name", string value = "value", bool setDomain = false)
        {
            var httpCookie = new HttpCookie(name, value);

            if (setDomain)
            {
                httpCookie.Domain = domain;
            }

            Response.SetCookie(httpCookie);
        }

        public string GetCookieForm(string name = "name")
        {
            return Request[name];
        }

        public string GetCookie(string name = "name")
        {
            return Request.Cookies[name]?.Value;
        }

        public void ClearCookie(string domain = "musync.ru", string name = "name", bool setDomain = false)
        {
            var httpCookie = new HttpCookie(name, string.Empty)
                {Expires = new DateTime(2000, 1, 1)};

            if (setDomain)
            {
                httpCookie.Domain = domain;
            }

            Response.SetCookie(httpCookie);
        }

        public void ClearSetCookie(string domain = "musync.ru", string name = "name", string value = "value")
        {
            var httpCookie = new HttpCookie(name, string.Empty) {Expires = new DateTime(2000, 1, 1)};

            Response.SetCookie(httpCookie);

            var httpCookie2 = new HttpCookie(name, value) { Domain = domain };

            Response.SetCookie(httpCookie2);
        }

        public void Ping()
        {
        }

        public void GetInputData()
        {
            WriteResponse(new Input
            {
                K = 10,
                Muls = new[] {1, 4},
                Sums = new[] {1.01m, 2.02m}
            });
        }

        [HttpGet]
        public async Task Sleep(int milliseconds = 2000)
        {
            var sw = new Stopwatch();

            sw.Start();
            await Task.Delay(milliseconds).ConfigureAwait(true);
            sw.Stop();

            WriteResponse(sw.ElapsedMilliseconds);
        }

        public void WriteAnswer()
        {
            var answer = GetFromBody<Output>();
        }

        [HttpGet]
        public ActionResult Help()
        {
            return PartialView();
        }

        private static readonly Random random = new Random();
        private Random Random => random ?? new Random();
        
        public void RandomError(int probability)
        {
            ProcessErrorProbability(probability);
        }

        public void RandomError50()
        {
            ProcessErrorProbability(50);
        }

        public void RandomError90()
        {
            ProcessErrorProbability(90);
        }

        public void RandomError0()
        {
            ProcessErrorProbability(0);
        }

        public void RandomError10()
        {
            ProcessErrorProbability(10);
        }

        private void ProcessErrorProbability(int probability)
        {
            if (probability > 100)
            {
                probability = 100;
            }

            if (probability < 0)
            {
                probability = 0;
            }

            var chance = Random.Next(0, 100);
            if (chance <= probability)
            {
                throw new HttpException(429, $"It's error with {probability}% probability!");
            }

            WriteResponse($"Ok! Error with {probability}% has not occured.");
        }

        [HttpGet]
        public string CalcMetronom(string time, int takts)
        {
            var split = time.Split(new[] {":"}, StringSplitOptions.RemoveEmptyEntries);
            var min = split.Length == 2 ? int.Parse(split[0]) : 0;
            var sec = split.Length == 2 ? decimal.Parse(split[1]) : decimal.Parse(split[0]);
            var secs = min * 60 + sec;
            return (takts * 4 / (secs / 60)).ToString();
        }

        [HttpGet]
        public void Find(string key)
        {
            using (var queryExecutor = queryExecutorFactory.Create())
            {
                var keyValue = queryExecutor.Execute<IKeyValueHandler, KeyValue>(h => h.Find(key));
                WriteResponse(keyValue);
            }
        }

        [HttpGet]
        public void Select(string[] keys)
        {
            using (var queryExecutor = queryExecutorFactory.Create())
            {
                var keyValue = queryExecutor.Execute<IKeyValueHandler, KeyValue[]>(h => h.Select(keys));
                WriteResponse(keyValue);
            }
        }

        [HttpPost]
        public void Create()
        {
            var keyValue = GetFromBody<KeyValue>();
            using (var queryExecutor = queryExecutorFactory.Create())
            {
                if (queryExecutor.Execute<IKeyValueHandler, KeyValue>(h => h.Find(keyValue.Key)) != null)
                    throw new HttpException((int) HttpStatusCode.BadRequest,
                        $"Key {keyValue.Key} is already presented in store.");
                queryExecutor.Execute<IKeyValueHandler>(h => h.Create(keyValue));
            }
        }

        [HttpPost]
        public void CreateAll()
        {
            var keyValues = GetFromBody<KeyValue[]>();
            using (var queryExecutor = queryExecutorFactory.Create())
            {
                foreach (var keyValue in keyValues)
                {
                    if (queryExecutor.Execute<IKeyValueHandler, KeyValue>(h => h.Find(keyValue.Key)) != null)
                        throw new HttpException((int) HttpStatusCode.BadRequest,
                            $"Key {keyValue.Key} is already presented in store.");
                }

                queryExecutor.Execute<IKeyValueHandler>(h => h.Create(keyValues));
            }
        }

        [HttpPost]
        public void Update(string key, string value)
        {
            using (var queryExecutor = queryExecutorFactory.Create())
            {
                var existed = queryExecutor.Execute<IKeyValueHandler, KeyValue>(h => h.Find(key));
                if (existed == null)
                    throw new HttpException((int) HttpStatusCode.BadRequest, $"Key {key} is not presented in store.");
                existed.Value = value;
                queryExecutor.Execute<IKeyValueHandler>(h => h.Update(existed));
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            log.Error(filterContext.Exception?.Message);

            if (filterContext.ExceptionHandled)
                return;

            try
            {
                if (!(filterContext.Exception is HttpException))
                {
                    throw new HttpException((int) HttpStatusCode.InternalServerError, "Oops...Somthing was wrong.",
                        filterContext.Exception);
                }

                throw filterContext.Exception;
            }
            catch (HttpException ex)
            {
                HttpContext.Response.StatusCode = ex.GetHttpCode();
                var bytes = Encoding.UTF8.GetBytes(ex.Message);
                Response.OutputStream.Write(bytes, 0, bytes.Length);
                log.Error(ex.Message, ex);
            }

            filterContext.ExceptionHandled = true;
            ControllerContext.HttpContext.Response.TrySkipIisCustomErrors = true;
        }

        private T GetFromBody<T>()
        {
            try
            {
                Request.InputStream.Seek(0, SeekOrigin.Begin);

                byte[] body;
                using (var streamReader = new StreamReader(Request.InputStream))
                {
                    body = Encoding.UTF8.GetBytes(streamReader.ReadToEnd());
                }


                var contentType = Request.ContentType;
                return contentType == xmlContentType
                    ? xmlSerialiazer.Deserialize<T>(body)
                    : jsonSerializer.Deserialize<T>(body);
            }
            catch (Exception)
            {
                throw new HttpException((int) HttpStatusCode.BadRequest, "Wrong parameters.");
            }
        }

        private void WriteResponse<T>(T response)
        {
            var realContentType = Request.AcceptTypes != null && Request.AcceptTypes.Contains(xmlContentType)
                ? xmlContentType
                : jsonContentType;
            var bytes = realContentType == xmlContentType
                ? xmlSerialiazer.Serialize(response)
                : jsonSerializer.Serialize(response);
            Response.AppendHeader("Content-Type", realContentType);
            Response.OutputStream.Write(bytes, 0, bytes.Length);
        }
    }
}