﻿using System;
using System.Data.Linq.Mapping;
using Newtonsoft.Json;

namespace Tolltech.MusyncUI.Study
{
    [Table(Name = "KeyValues")]
    public class KeyValue
    {
        [JsonIgnore]
        [Column(Name = "Id", DbType = "uniqueidentifier", IsPrimaryKey = true, CanBeNull = false)]
        public Guid Id { get; set; }

        [Column(Name = "Key", DbType = "nvarchar(MAX)", CanBeNull = false)]
        public string Key { get; set; }

        [Column(Name = "Value", DbType = "nvarchar(MAX)", CanBeNull = false)]
        public string Value { get; set; }
    }
}