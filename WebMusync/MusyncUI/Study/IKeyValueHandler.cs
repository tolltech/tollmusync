﻿namespace Tolltech.MusyncUI.Study
{
    public interface IKeyValueHandler
    {
        KeyValue Find(string key);
        void Create(params KeyValue[] keyValues);
        KeyValue[] Select(string[] keys);
        void Update(params object[] existed);
    }
}