﻿namespace Tolltech.MusyncUI.Models.ApiModels
{
    public class ApiResult
    {
        public bool Success { get; set; }
        public string Error { get; set; }

        protected ApiResult()
        {
        }

        public static ApiResult CreateSuccess()
        {
            return new ApiResult
                {
                    Error = string.Empty,
                    Success = true
                };
        }

        public static ApiResult CreateFail(string error)
        {
            return new ApiResult
            {
                Error = error,
                Success = false
            };
        }
    }
}