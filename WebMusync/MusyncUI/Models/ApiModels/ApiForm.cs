﻿using System;

namespace Tolltech.MusyncUI.Models.ApiModels
{
    public class ApiForm<T>
    {
        public T Form { get; set; }
        public Guid TokenId { get; set; }
    }
}