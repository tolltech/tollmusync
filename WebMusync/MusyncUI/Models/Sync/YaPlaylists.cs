﻿using System;

namespace Tolltech.MusyncUI.Models.Sync
{
    public class YaPlaylists
    {
        public YaPlaylists()
        {
            Playlists = Array.Empty<YaPlaylist>();
        }

        public YaPlaylist[] Playlists { get; set; }
    }
}