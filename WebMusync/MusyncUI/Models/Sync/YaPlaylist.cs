﻿namespace Tolltech.MusyncUI.Models.Sync
{
    public class YaPlaylist
    {
        public string Title { get; set; }
        public string Id { get; set; }
    }
}