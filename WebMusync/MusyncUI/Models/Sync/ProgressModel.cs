﻿using System;

namespace Tolltech.MusyncUI.Models.Sync
{
    public class ProgressModel
    {
        public Guid Id { get; set; }
        public int Total { get; set; }
        public int Proecssed { get; set; }
    }
}