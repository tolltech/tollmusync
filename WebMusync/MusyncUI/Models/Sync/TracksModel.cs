﻿using System.Collections.Generic;

namespace Tolltech.MusyncUI.Models.Sync
{
    public class TracksModel
    {
        public TracksModel()
        {
            Tracks = new List<TrackModel>();
        }

        public List<TrackModel> Tracks { get; set; }
    }
}