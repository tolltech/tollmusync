﻿using System.ComponentModel.DataAnnotations;

namespace Tolltech.MusyncUI.Models
{
    public class FeedbackForm
    {
        [Display(Name = "Заголовок")]
        public string Title { get; set; }
        [Display(Name = "Собственно, текст")]
        public string Text { get; set; }
        [Display(Name = "Ваша почта, если хотите")]
        public string Email { get; set; }
    }
}