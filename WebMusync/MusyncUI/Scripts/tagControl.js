﻿function HashTags(selector, tagMenuId) {
    var tagsTextboxes = $(selector);
    tagsTextboxes.attr("autocomplete", "off");
    var currentTagInput;
    tagsTextboxes.keyup(function (evt) {

        var keyCode = evt.keyCode;
        if (keyCode == 40) {
            tagsSelect(evt);
            return;
        }

        var valid =
            (keyCode > 47 && keyCode < 58) || // number keys
            keyCode == 32 ||  // spacebar & return key(s) (if you want to allow carriage returns)
                keyCode == 8 ||
            (keyCode > 64 && keyCode < 91) || // letter keys
            (keyCode > 95 && keyCode < 112) || // numpad keys
            (keyCode > 185 && keyCode < 193) || // ;=,-./` (in order)
            (keyCode > 218 && keyCode < 223);   // [\]' (in order)        

        if (!valid)
            return;

        var input = $(evt.srcElement);
        currentTagInput = input;

        var currentText = currentTagInput.val();
        var ind = getLastIndexOf(currentText, ',');
        var lastTag = currentText.substring(ind + 1);
        $.post("/Note/GetLastHashTag", { start: lastTag }, getTagsSuccess);
    });
    tagsTextboxes.click(function (evt) {
        var input = $(evt.srcElement);
        currentTagInput = input;

        //if (!input.val())
        //    return;

        var currentText = currentTagInput.val();
        var ind = getLastIndexOf(currentText, ',');
        var lastTag = currentText.substring(ind + 1);

        $.post("/Note/GetLastHashTag", { start: lastTag }, getTagsSuccess);
    });

    function getLastIndexOf(text, ch) {
        var ind = -1;
        for (var i = text.length - 1 ; i >= 0; --i) {
            if (text[i] == ch) {
                ind = i;
                break;
            }
        }
        return ind;
    }

    function selectTag(evt) {
        var src = $(evt.srcElement);
        var text = src.html();
        addText(text);

        var list = $('#' + tagMenuId);
        list.css("display", "none");
    }

    function addText(text) {
        var currentText = currentTagInput.val();

        var ind = getLastIndexOf(currentText, ',');
        var safeText = currentText.substring(0, ind + 1);
        safeText = safeText + text + ", ";
        currentTagInput.val(safeText);
    }

    function tagsSelect(evt) {
        if (evt.keyCode == 13) {
            selectTag(evt);
            currentTagInput.focus();
            return;
        }

        if (evt.keyCode != 38 && evt.keyCode != 40)
            return;

        var toDown = evt.keyCode == 40;
        var el = $(evt.srcElement);
        var ind;
        var items = $("#" + tagMenuId + " .tagListItem");
        if (!items.length)
            return;

        if (el.prop("tagName") == "INPUT") {
            ind = 0;
        } else {
            var number = parseInt(el.attr("number"));
            if (toDown) {
                if (number >= (items.length - 1)) {
                    ind = 0;
                } else {
                    ind = number + 1;
                }
            } else {
                if (number == 0) {
                    ind = -1;
                } else {
                    ind = number - 1;
                }
            }
            $(items[number]).removeClass("tagListItemhover");
        }

        //console.log("attempting to write index " + ind);
        evt.stopImmediatePropagation();

        if (ind == -1) {
            currentTagInput.focus();
            return;
        }

        $(items[ind]).addClass("tagListItemhover");
        $(items[ind]).focus();
    }

    function getTagsSuccess(data) {
        var tags = eval(data);

        //if (!tags || !tags.length) {
        //    srcElement.attr("magicAttr", "true");
        //    list.css("display", "block");
        //    return;
        //}

        var list = $('#' + tagMenuId);

        list.html("");
        for (var i = 0; i < tags.length; ++i) {
            var e = $("<div></div>");
            e.addClass("tagListItem");
            e.attr("tabindex", "-1");
            e.attr("number", i);
            e.append(tags[i]);

            e.hover(function (evt) {
                $(".tagListItem").removeClass("tagListItemhover");
                $(evt.srcElement).addClass("tagListItemhover");
                this.focus();
            }, function () {
                $(".tagListItem").removeClass("tagListItemhover");
                this.blur();
            }).keydown(function (evt) {
                tagsSelect(evt);
                evt.stopImmediatePropagation();
                return false;
            }).click(function (evt) {
                selectTag(evt);
                currentTagInput.focus();
            });

            list.append(e);
        }

        //todo: забиндить
        $(".tagListItem").click(function (evt) {
            return false;
        });

        currentTagInput.attr("magicAttr", "true");

        list.css("display", "block");
        var width = currentTagInput.outerWidth() - parseInt(list.css("border-left-width")) - parseInt(list.css("border-right-width"));
        list.width(width);
        var top = currentTagInput.offset().top + currentTagInput.outerHeight();
        var left = currentTagInput.offset().left;
        list.offset({ top: top, left: left });
    }

    $(document).ready(function () {
        $(document).click(function (evt) {
            var src = $(evt.srcElement);
            if (src.attr("magicAttr"))
                return;
            $(selector).attr("magicAttr", "");
            var list = $('#' + tagMenuId);
            list.css("display", "none");
        });
    });
}