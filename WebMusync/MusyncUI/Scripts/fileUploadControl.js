﻿function InitFileUploads() {
    var inputUploads = $("input.fileUpload");
    inputUploads.on('change', function (evt, data) {
        handleFileSelect(evt);
    });

    var fr = null;
    function handleFileSelect(evt) {
        if (window.File && window.FileReader && window.FileList && window.Blob) {

        } else {
            alert('The File APIs are not fully supported in this browser.');
            return;
        }

        var input = $(evt.srcElement)[0];
        var formId = $(input).closest("form").attr("id");
        if (!input) {
            alert("Um, couldn't find the fileinput element.");
        }
        else if (!input.files) {
            alert("This browser doesn't seem to support the `files` property of file inputs.");
        }
        else if (!input.files[0]) {
            alert("Please select a file before clicking 'Load'");
        }
        else {
            var file = input.files[0];
            fr = new FileReader();
            fr.onload = function () {
                $.post("/Note/Upload", { formId: formId, file: fr.result }, uploadSuccess);
            };
            fr.readAsDataURL(file);
        }
    }

    function uploadSuccess(data) {
        var result = $.parseJSON(data);
        var imgDiv = $('#' + result.formId + " .divUploadImg");
        imgDiv.append("<img src='" + result.Path + "' class='noteImg' />");
        var imgInput = $("input[name=Images]");
        var newImg = result.Id;
        var val = imgInput.attr("value");
        if (val)
            val = val + ",";
        val = val + newImg;
        imgInput.attr("value", val);

        var inputFile = $('#' + result.formId + " .fileUpload");
        inputFile.val('');
    }
}