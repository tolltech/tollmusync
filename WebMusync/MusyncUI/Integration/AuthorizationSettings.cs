﻿using System;
using System.Collections.Concurrent;
using Tolltech.Musync.Settings;

namespace Tolltech.MusyncUI.Integration
{
    public class AuthorizationSettings : IAuthorizationSettings
    {
        private static readonly ConcurrentDictionary<Guid, MuserAuthorization> authorizations =
            new ConcurrentDictionary<Guid, MuserAuthorization>();

        public MuserAuthorization GetAndCacheMuserAuthorization(string cryptoKey, Guid? userId = null)
        {
            if (!userId.HasValue)
            {
                throw new NotSupportedException("Authorize!");
            }

            return authorizations.TryGetValue(userId.Value, out var authorization) ? authorization : null;
        }

        public void SetMuserAuthorization(string cryptoKey, MuserAuthorization authorization, Guid? userId = null)
        {
            if (!userId.HasValue)
            {
                throw new NotSupportedException("Authorize!");
            }

            authorizations.AddOrUpdate(userId.Value, authorization, (guid, muserAuthorization) => authorization);
        }

        public MuserAuthorization GetCachedMuserAuthorization(Guid userId)
        {
            return authorizations.TryGetValue(userId, out var authorization) ? authorization : null;
        }

        public bool UserAuthorized(Guid userId)
        {
            return authorizations.ContainsKey(userId);
        }

        public bool HasStoredAuthorizartion => throw new NotSupportedException("Authorize!");
        public MuserAuthorization MuserAuthorization => throw new NotSupportedException("Authorize!");
        public bool Authorized => throw new NotSupportedException("Authorize!");
    }
}