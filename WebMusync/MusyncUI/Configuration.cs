﻿using System.Configuration;
using Tolltech.ThisCore;

namespace Tolltech.MusyncUI
{
    public class Configuration : IConfiguration
    {
        public string Get(string key)
        {
            return ((MusyncUIConfigurationSection)ConfigurationManager.GetSection(ConfigKeys.ServiceSectionNameKey)).ElementInformation.Properties[key].Value.ToString();
        }
    }
}