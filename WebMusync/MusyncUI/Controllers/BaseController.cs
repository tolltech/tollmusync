﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Web.Mvc;
using System.Web.Security;
using log4net;
using Tolltech.MusyncUI.Filters;
using Tolltech.MusyncUI.Implementaitions.Users;
using RequestContext = System.Web.Routing.RequestContext;

namespace Tolltech.MusyncUI.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public abstract class BaseController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(BaseController));
        private static readonly ConcurrentDictionary<string, Guid> users = new ConcurrentDictionary<string, Guid>();
        private readonly INoteUserService noteUserService;

        protected BaseController(INoteUserService noteUserService)
        {
            this.noteUserService = noteUserService;
        }

        protected bool IsAuthentificated => User.Identity?.IsAuthenticated ?? false;

        protected Guid? GetUserId()
        {
            var userName = User.Identity.Name;
            var user = noteUserService.Find(userName);
            return user?.Id;
        }

        protected string RootPath => Request.RequestContext.HttpContext.Server.MapPath("~");

        protected Guid UserId
        {
            get
            {
                return users.GetOrAdd(User.Identity.Name, x => noteUserService.Read(x).Id);
            }
        }

        protected string GetUserPassword()
        {
            var user = Membership.GetUser(User.Identity.Name, false);
            return user?.GetPassword();
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            var msg = GetRequestInfo(filterContext.RequestContext);
            log.Info(WebUtility.UrlDecode("Start" + msg));
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var msg = GetRequestInfo(filterContext.RequestContext);

            var response = filterContext.HttpContext.Response;

            log.Info(WebUtility.UrlDecode("End " + msg + $"\r\n With Result {response.StatusCode} {response.Status} {response.StatusDescription} Canceled = {filterContext.Canceled} Exception = {filterContext.Exception?.Message}"
                                          ));

            base.OnActionExecuted(filterContext);
        }

        private static string GetRequestInfo(RequestContext filterContext)
        {
            var msg = $"Request to {filterContext.HttpContext.Request.RawUrl}";
            return msg;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            log.Error("Controller exception.", filterContext.Exception);
            //If the exeption is already handled we do nothing
            if (filterContext.ExceptionHandled)
            {
                return;
            }

            if (filterContext.RequestContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                filterContext.HttpContext.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                filterContext.Result = new JsonResult
                {
                    Data = filterContext.Exception?.Message
                };
            }
            else
            {
                filterContext.Result = View("Error");
            }           

            //Make sure that we mark the exception as handled
            filterContext.ExceptionHandled = true;
        }
    }
}