﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.Web.WebPages.OAuth;
using Tolltech.MusyncUI.Filters;
using Tolltech.MusyncUI.Implementaitions.Mail;
using Tolltech.MusyncUI.Implementaitions.Users;
using Tolltech.MusyncUI.Models;
using Tolltech.ThisCore.Mail;
using WebMatrix.WebData;

namespace Tolltech.MusyncUI.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class AccountController : BaseController
    {
        private readonly INoteUserService noteUserService;
        private readonly IMailClient mailClient;
        private readonly IMailMessageService mailMessageService;

        public AccountController(INoteUserService noteUserService, IMailClient mailClient, IMailMessageService mailMessageService)
            : base(noteUserService)
        {
            this.noteUserService = noteUserService;
            this.mailClient = mailClient;
            this.mailMessageService = mailMessageService;
        }

        //
        // GET: /Account/Login

        [System.Web.Mvc.AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                return RedirectToLocal(returnUrl);
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Неверное имя пользователя или пароль.");
            return View(model);
        }

        //
        // POST: /Account/LogOff

        [System.Web.Mvc.HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register

        [System.Web.Mvc.AllowAnonymous]
        public ActionResult Register(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                return View("RegisterStart");
            }
            var urlTokenDecode = HttpServerUtility.UrlTokenDecode(token);
            var tokenId = new Guid(urlTokenDecode);
            var user = noteUserService.FindByToken(tokenId);
            if (user != null)
                return View("Register", new RegisterModel { UserName = user.Email, TokenId = user.TokenId });
            return View("RegisterStart");
        }

        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterStart(RegisterStartModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    var existsUser = noteUserService.Find(model.UserName);

                    var user = existsUser ?? noteUserService.Create(model.UserName);

                    mailMessageService.Create(user.Email, "Активация аккаунта", string.Format("Для активации аккаунта перейдите по ссылке http://{0}/Account/Register?token={1}",
                            HttpContext.Request.Url.Host,
                            HttpServerUtility.UrlTokenEncode(user.TokenId.ToByteArray())));

                    mailClient.Send(user.Email,
                        $"Для активации аккаунта перейдите по ссылке http://{HttpContext.Request.Url.Host}/Account/Register?token={HttpServerUtility.UrlTokenEncode(user.TokenId.ToByteArray())}", "Активация аккаунта");
                    return View("TokenSent");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // If we got this far, something failed, redisplay form
            return View("RegisterStart", model);
        }

        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUser(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    var existsUser = noteUserService.Find(model.UserName);
                    if (existsUser == null)
                    {
                        ModelState.AddModelError("", ErrorCodeToString(MembershipCreateStatus.UserRejected));
                        return View("Register", model);
                    }
                    if (existsUser.TokenId != model.TokenId)
                    {
                        ModelState.AddModelError("", ErrorCodeToString(MembershipCreateStatus.UserRejected));
                        return View("Register", model);
                    }

                    WebSecurity.CreateUserAndAccount(model.UserName, model.Password);
                    WebSecurity.Login(model.UserName, model.Password);
                    noteUserService.ActivateUser(existsUser.Id);
                    return RedirectToAction("Index", "Home");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // If we got this far, something failed, redisplay form
            return View("Register", model);
        }

        //
        // POST: /Account/Register

        //
        // POST: /Account/Disassociate      

        //
        // GET: /Account/Manage

        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Пароль успешно изменен."
                : message == ManageMessageId.SetPasswordSuccess ? "Пароль установлен."
                : message == ManageMessageId.RemoveLoginSuccess ? "Внешний логин удален."
                : "";
            ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage

        [System.Web.Mvc.HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(LocalPasswordModel model)
        {
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;
                    try
                    {
                        changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        ModelState.AddModelError("", "Текущий пароль неверен.");
                    }
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", e);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [System.Web.Mvc.AllowAnonymous]
        public ActionResult ResetPasswordFinally(ResetPasswordByTokenModel model)
        {
            if (ModelState.IsValid)
            {
                // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                var changePasswordSucceeded = true;
                try
                {
                    WebSecurity.ResetPassword(model.Token, model.Password);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Неверные апарметры запроса.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View("ResetPasswordByToken", model);
        }

        [System.Web.Mvc.AllowAnonymous]
        public ActionResult ResetPassword()
        {
            return View();
        }

        [System.Web.Mvc.AllowAnonymous]
        public ActionResult ResetByToken(string token)
        {
            return View("ResetPasswordByToken", new ResetPasswordByTokenModel { Token = token });
        }

        [System.Web.Mvc.AllowAnonymous]
        public ActionResult SendResetToken(ResetPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var userName = model.UserName;
                var user = noteUserService.Find(userName);
                if (user != null)
                {
                    bool generteTpken;
                    var resetToken = "";
                    try
                    {
                        resetToken = WebSecurity.GeneratePasswordResetToken(userName);
                        generteTpken = true;
                    }
                    catch (Exception)
                    {
                        generteTpken = false;
                    }

                    if (generteTpken)
                    {
                        mailMessageService.Create(userName, "Сброс пароля",
                            $"Для смены пароля перейдите по ссылке http://{HttpContext.Request.Url.Host}/Account/ResetByToken?token={resetToken}");

                        mailClient.Send(userName,
                            $"Для смены пароля перейдите по ссылке http://{HttpContext.Request.Url.Host}/Account/ResetByToken?token={resetToken}", "Сброс пароля");
                    }
                }

                return View("TokenSent");
            }
            return View("ResetPassword");
        }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Пользователь с таким именем уже существует. Пожалуйста, выберите другое.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "Пользователь с таким именем уже существует. Пожалуйста, выберите другое.";

                case MembershipCreateStatus.InvalidPassword:
                    return "Некорректный формат пароля.";

                case MembershipCreateStatus.InvalidEmail:
                    return "Некорректный формат почты.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "Некорректный формат имени пользователя.";

                case MembershipCreateStatus.ProviderError:
                    return "Некорректные имя пользователя или пароль.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "Неизвестная ошибка.";
            }
        }
        #endregion
    }
}
