﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Tolltech.Musync.Domain;
using Tolltech.Musync.Models;
using Tolltech.Musync.Settings;
using Tolltech.MusyncUI.Filters;
using Tolltech.MusyncUI.Implementaitions;
using Tolltech.MusyncUI.Implementaitions.Common;
using Tolltech.MusyncUI.Implementaitions.Extensions;
using Tolltech.MusyncUI.Implementaitions.Sync;
using Tolltech.MusyncUI.Implementaitions.Users;
using Tolltech.MusyncUI.Models.Sync;

namespace Tolltech.MusyncUI.Controllers
{
    [NoCache]
    public class SyncController : BaseController
    {
        private readonly ITrackGetter trackGetter;
        private readonly IYandexService yandexService;
        private readonly IAuthorizationSettings authorizationSettings;
        private readonly IDomainService domainService;
        private readonly IProgressBar progressBar;
        private readonly IImportResultLogger importResultLogger;

        public SyncController(ITrackGetter trackGetter, IYandexService yandexService,
            IAuthorizationSettings authorizationSettings, INoteUserService noteUserService,
            IDomainService domainService,
            IProgressBar progressBar,
            IImportResultLogger importResultLogger) : base(noteUserService)
        {
            this.trackGetter = trackGetter;
            this.yandexService = yandexService;
            this.authorizationSettings = authorizationSettings;
            this.domainService = domainService;
            this.progressBar = progressBar;
            this.importResultLogger = importResultLogger;
        }

        [HttpPost]
        public ActionResult AuthorizeVk(AuthorizeForm authorizeForm)
        {
            var authInfo = authorizationSettings.GetCachedMuserAuthorization(UserId);
            if (authInfo == null)
            {
                authInfo = new MuserAuthorization
                {
                    VkLogin = authorizeForm.Login,
                    VkPassword = authorizeForm.Pass
                };
            }
            else
            {
                authInfo.VkLogin = authorizeForm.Login;
                authInfo.VkPassword = authorizeForm.Pass;
            }

            authorizationSettings.SetMuserAuthorization(string.Empty, authInfo, UserId);


            return PartialView("AuthorizeResult", new AuthorizeResult {Success = true});
        }

        [HttpPost]
        public void UnauthorizeYa()
        {
            var authInfo = authorizationSettings.GetCachedMuserAuthorization(UserId);
            if (authInfo == null)
            {
                return;
            }
            else
            {
                authInfo.YaLogin = string.Empty;
                authInfo.YaPassword = string.Empty;
            }

            authorizationSettings.SetMuserAuthorization(string.Empty, authInfo, UserId);
        }

        [HttpPost]
        public async Task<ActionResult> AuthorizeYa(AuthorizeForm authorizeForm)
        {
            var success = await yandexService.CheckCredentialsAsync(authorizeForm.Login, authorizeForm.Pass)
                .ConfigureAwait(true);

            Response.SetCookies(Constants.YaLoginCookie, authorizeForm.Login ?? string.Empty);

            if (!success)
            {
                return PartialView("AuthorizeResult", new AuthorizeResult {Success = false});
            }

            var authInfo = authorizationSettings.GetCachedMuserAuthorization(UserId);
            if (authInfo == null)
            {
                authInfo = new MuserAuthorization
                {
                    YaLogin = authorizeForm.Login,
                    YaPassword = authorizeForm.Pass
                };
            }
            else
            {
                authInfo.YaLogin = authorizeForm.Login;
                authInfo.YaPassword = authorizeForm.Pass;
            }

            authorizationSettings.SetMuserAuthorization(string.Empty, authInfo, UserId);

            return await GetYaPlaylists().ConfigureAwait(true);
        }

        [HttpGet]
        public async Task<ActionResult> GetVkTracks(VkForm form)
        {
            var tracks = await trackGetter.GetTracksAsync(form.AudioString).ConfigureAwait(true);

            Response.SetCookies(Constants.VkUrlCookie, form.AudioString ?? string.Empty);

            return PartialView("Tracks", tracks);
        }

        [HttpGet]
        public async Task<ActionResult> GetNewVkTracks(string audioStr, string yaPlaylistId)
        {
            var audioUrl = trackGetter.GetAudioUrl(audioStr);
            var tracks = await domainService.GetNewVkTracksUnauthorizedAsync(yaPlaylistId, UserId, audioUrl).ConfigureAwait(true);
            return PartialView("Tracks", tracks.ToTracksModel());
        }


        [HttpPost]
        public async Task<ActionResult> ImportTracks(TrackImportForm tracksForm)
        {
            var trackToImport = tracksForm.Tracks.Tracks
                .Select(x => new VkTrack
                {
                    Artist = x.Artist,
                    Title = x.Title,
                })
                .Reverse()
                .ToArray();

            var progressId = Guid.NewGuid();

            var results = await domainService.ImportTracksAsync(trackToImport, tracksForm.YaPlaylistId, UserId, tuple =>
                progressBar.UpdateProgressModel(new ProgressModel
                {
                    Id = progressId,
                    Proecssed = tuple.Processed,
                    Total = tuple.Total
                })).ConfigureAwait(true);

            importResultLogger.WriteImportLogs(results, UserId);

            return PartialView("Progress", progressBar.GetProgressModel(progressId));
        }

        [HttpGet]
        public ActionResult GetProgress(Guid progressId)
        {
            return PartialView("Progress", progressBar.GetProgressModel(progressId));
        }

        [HttpGet]
        public async Task<ActionResult> GetYaPlaylists()
        {
            var client = await yandexService.GetClientAsync(UserId).ConfigureAwait(true);

            var playlists = await client.GetPlaylistsAsync().ConfigureAwait(true);

            return PartialView("YaPlaylists", new YaPlaylists
            {
                Playlists = playlists
                    .Select(x => new YaPlaylist
                    {
                        Title = x.Title,
                        Id = x.Id
                    })
                    .ToArray()
            });
        }

        [HttpGet]
        public async Task<ActionResult> GetYaTracks(string yaPlaylistId)
        {
            var client = await yandexService.GetClientAsync(UserId).ConfigureAwait(true);

            var tracks = await client.GetTracksAsync(yaPlaylistId).ConfigureAwait(true);

            return PartialView("Tracks", tracks.ToTracksModel());
        }
    }
}