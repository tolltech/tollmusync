﻿using System.Web.Mvc;
using Tolltech.MusyncUI.Implementaitions.Feedback;
using Tolltech.MusyncUI.Implementaitions.Users;
using Tolltech.MusyncUI.Models;

namespace Tolltech.MusyncUI.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IFeedbackService feedbackService;

        public HomeController(INoteUserService noteUserService, IFeedbackService feedbackService)
            : base(noteUserService)
        {
            this.feedbackService = feedbackService;
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            if (IsAuthentificated)
            {
                return View("Work");
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        [AllowAnonymous]
        public ActionResult NeedMoney()
        {
            return View("NeedMoney");
        }

        [AllowAnonymous]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [AllowAnonymous]
        public string Feedback(FeedbackForm form)
        {
            feedbackService.Create(form.Text, form.Title, form.Email, GetUserId());
            return "Спасибо, ваше сообщение не останется без внимания.";
        }
    }
}
