﻿using System.Configuration;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Xml;
using log4net.Config;
using Ninject;
using Ninject.Web.Common;
using Tolltech.Musync.Settings;
using Tolltech.MusyncUI.Implementaitions;
using Tolltech.ThisCore;
using Tolltech.ThisCore.Sql;
using AuthorizationSettings = Tolltech.MusyncUI.Integration.AuthorizationSettings;

namespace Tolltech.MusyncUI
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : NinjectHttpApplication
    {
        protected override void OnApplicationStarted()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            var section = (XmlElement)ConfigurationManager.GetSection("log4net");
            XmlConfigurator.Configure(section);

            ControllerBuilder.Current.SetControllerFactory(typeof(NinjectControllerFactory));
        }

        protected override IKernel CreateKernel()
        {
            var ninjectKernel = new StandardKernel();
            Load(ninjectKernel);
            AddBindings(ninjectKernel);
            return ninjectKernel;
        }

        private static void AddBindings(IKernel ninjectKernel)
        {
            IoCResolver.Resolve((@interface, implementation) => ninjectKernel.Bind(@interface).To(implementation),
                "Tolltech");

            ninjectKernel.Rebind<IAuthorizationSettings>().To<AuthorizationSettings>();
        }

        private static void Load(IKernel kernel)
        {
            kernel.Load(
                Assembly.GetExecutingAssembly(),
                Assembly.GetAssembly(typeof(IQueryExecutorFactory)));
        }
    }
}