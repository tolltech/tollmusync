﻿using System.Configuration;
using Tolltech.ThisCore;

namespace Tolltech.MusyncUI
{
    public class MusyncUIConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty(ConfigKeys.DbConnectionStringKey, DefaultValue = "", IsKey = false, IsRequired = true)]
        public string DBConnectionString { get; set; }
    }
}