﻿using System;
using System.Data.Linq.Mapping;

namespace Tolltech.MusyncUI.Implementaitions.Users
{
    [Table(Name = "NoteUsers")]
    public class NoteUser
    {
        [Column(Name = "Id", DbType = "uniqueidentifier", IsPrimaryKey = true, CanBeNull = false)]
        public Guid Id { get; set; }

        [Column(Name = "Email", DbType = "nvarchar(1024)", CanBeNull = false)]
        public string Email { get; set; }

        [Column(Name = "TokenId", DbType = "uniqueidentifier", CanBeNull = false)]
        public Guid TokenId { get; set; }

        [Column(Name = "IsActive", DbType = "bit", CanBeNull = false)]
        public bool IsActive { get; set; }

        [Column(Name = "CreateDate", DbType = "dateTime", CanBeNull = false)]
        public DateTime CreateDate { get; set; }
    }
}