﻿using Tolltech.ThisCore.Common;

namespace Tolltech.MusyncUI.Implementaitions.Users
{
    public class UserFactory : IUserFactory
    {
        private readonly IGuidGenerator guidGenerator;
        private readonly IDateTimeService dateTimeService;

        public UserFactory(IGuidGenerator guidGenerator, IDateTimeService dateTimeService)
        {
            this.guidGenerator = guidGenerator;
            this.dateTimeService = dateTimeService;
        }

        public NoteUser Create(string email)
        {
            return new NoteUser
                {
                    Id = guidGenerator.Create(),
                    Email = email,
                    IsActive = false,
                    TokenId = guidGenerator.Create(),
                    CreateDate = dateTimeService.Now
                };
        }
    }
}