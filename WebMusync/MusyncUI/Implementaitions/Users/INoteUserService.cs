﻿using System;

namespace Tolltech.MusyncUI.Implementaitions.Users
{
    public interface INoteUserService
    {
        NoteUser Create(string email);
        NoteUser Find(string email);
        NoteUser FindByToken(Guid tokenId);
        void ActivateUser(Guid userId);
        NoteUser Read(string email);

        NoteUser ReadByToken(Guid tokenId);
        Guid CreateOrGetTokenToUser(NoteUser noteUser);
    }
}