﻿using System;

namespace Tolltech.MusyncUI.Implementaitions.Users
{
    public interface IUserTokenFactory
    {
        UserToken Create(Guid userId);
    }
}