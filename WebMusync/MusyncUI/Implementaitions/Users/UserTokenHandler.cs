﻿using System;
using System.Linq;
using Tolltech.ThisCore.Common;
using Tolltech.ThisCore.Sql;

namespace Tolltech.MusyncUI.Implementaitions.Users
{
    public class UserTokenHandler : IUserTokenHandler
    {
        private readonly IDataContext dataContext;

        public UserTokenHandler(IDataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public void Create(UserToken token)
        {
            dataContext.Create(token);
        }

        public UserToken Read(Guid tokenId)
        {
            var token = dataContext.GetTable<UserToken>().FirstOrDefault(x => x.Id == tokenId);
            if (token == null)
                throw new EntityNotFoundException("unable to find userToken with Id" + tokenId.ToString());
            return token;
        }

        public UserToken Find(Guid userId)
        {
            return
                dataContext.GetTable<UserToken>()
                           .Where(x => x.UserId == userId)
                           .OrderByDescending(x => x.CreateDate)
                           .FirstOrDefault();
        }
    }
}