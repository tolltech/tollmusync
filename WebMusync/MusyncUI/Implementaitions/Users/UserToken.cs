﻿using System;
using System.Data.Linq.Mapping;

namespace Tolltech.MusyncUI.Implementaitions.Users
{
    [Table(Name = "UserTokens")]
    public class UserToken
    {
        [Column(Name = "Id", DbType = "uniqueidentifier", IsPrimaryKey = true, CanBeNull = false)]
        public Guid Id { get; set; }

        [Column(Name = "UserId", DbType = "uniqueidentifier", CanBeNull = false)]
        public Guid UserId { get; set; }

        [Column(Name = "CreateDate", DbType = "datetime", CanBeNull = false)]
        public DateTime CreateDate { get; set; }
    }
}