﻿namespace Tolltech.MusyncUI.Implementaitions.Users
{
    public interface IUserFactory
    {
        NoteUser Create(string email);
    }
}