﻿using System;
using System.Linq;
using Tolltech.ThisCore.Common;
using Tolltech.ThisCore.Sql;

namespace Tolltech.MusyncUI.Implementaitions.Users
{
    public class NoteUserHandler : INoteUserHandler
    {
        private readonly IDataContext dataContext;

        public NoteUserHandler(IDataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public void Create(NoteUser user)
        {
            dataContext.Create(user);
        }

        public NoteUser Find(string email)
        {
            return dataContext.GetTable<NoteUser>().FirstOrDefault(x => x.Email == email);
        }

        public NoteUser Find(Guid userId)
        {
            return dataContext.GetTable<NoteUser>().FirstOrDefault(x => x.Id == userId);
        }

        public NoteUser FindByToken(Guid tokenId)
        {
            return dataContext.GetTable<NoteUser>().FirstOrDefault(x => x.TokenId == tokenId);
        }

        public void Update(NoteUser user)
        {
            dataContext.Update();
        }

        public NoteUser Read(string email)
        {
            var user = dataContext.GetTable<NoteUser>().FirstOrDefault(x => x.Email == email);
            if (user == null)
                throw new EntityNotFoundException(string.Format("User {0} wasnt found", email));
            return user;
        }

        public NoteUser Read(Guid userId)
        {
            var user = dataContext.GetTable<NoteUser>().FirstOrDefault(x => x.Id == userId);
            if (user == null)
                throw new EntityNotFoundException(string.Format("User {0} wasnt found", userId));
            return user;
        }
    }
}