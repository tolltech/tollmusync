﻿using System;

namespace Tolltech.MusyncUI.Implementaitions.Users
{
    public interface IUserTokenHandler
    {
        void Create(UserToken token);
        UserToken Read(Guid tokenId);
        UserToken Find(Guid userId);
    }
}