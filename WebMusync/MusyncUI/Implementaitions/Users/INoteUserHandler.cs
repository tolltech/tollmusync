﻿using System;

namespace Tolltech.MusyncUI.Implementaitions.Users
{
    public interface INoteUserHandler
    {
        void Create(NoteUser user);
        NoteUser Find(string email);
        NoteUser Find(Guid userId);
        NoteUser FindByToken(Guid tokenId);
        void Update(NoteUser user);
        NoteUser Read(string email);
        NoteUser Read(Guid userId);
    }
}