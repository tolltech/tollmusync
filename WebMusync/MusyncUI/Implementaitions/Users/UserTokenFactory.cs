﻿using System;
using Tolltech.ThisCore.Common;

namespace Tolltech.MusyncUI.Implementaitions.Users
{
    public class UserTokenFactory : IUserTokenFactory
    {
        private readonly IGuidGenerator guidGenerator;
        private readonly IDateTimeService dateTimeService;

        public UserTokenFactory(IGuidGenerator guidGenerator, IDateTimeService dateTimeService)
        {
            this.guidGenerator = guidGenerator;
            this.dateTimeService = dateTimeService;
        }

        public UserToken Create(Guid userId)
        {
            return new UserToken
                {
                    Id = guidGenerator.Create(),
                    CreateDate = dateTimeService.Now,
                    UserId = userId
                };
        }
    }
}