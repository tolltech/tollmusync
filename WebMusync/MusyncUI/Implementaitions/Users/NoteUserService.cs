﻿using System;
using Tolltech.ThisCore.Mail;
using Tolltech.ThisCore.Sql;

namespace Tolltech.MusyncUI.Implementaitions.Users
{
    public class NoteUserService : INoteUserService
    {
        private readonly IQueryExecutorFactory queryExecutorFactory;
        private readonly IUserFactory userFactory;
        private readonly IMailClient mailClient;
        private readonly IUserTokenFactory userTokenFactory;

        public NoteUserService(IQueryExecutorFactory queryExecutorFactory, IUserFactory userFactory, IMailClient mailClient, IUserTokenFactory userTokenFactory)
        {
            this.queryExecutorFactory = queryExecutorFactory;
            this.userFactory = userFactory;
            this.mailClient = mailClient;
            this.userTokenFactory = userTokenFactory;
        }

        public NoteUser Create(string email)
        {
            using (var queryExecutor = queryExecutorFactory.Create())
            {
                var noteUser = userFactory.Create(email);
                queryExecutor.Execute<INoteUserHandler>(h => h.Create(noteUser));
                return noteUser;
            }
        }

        public NoteUser Find(string email)
        {
            using (var queryExecutor = queryExecutorFactory.Create())
            {
                return queryExecutor.Execute<INoteUserHandler, NoteUser>(h => h.Find(email));
            }
        }

        public NoteUser FindByToken(Guid tokenId)
        {
            using (var queryExecutor = queryExecutorFactory.Create())
            {
                return queryExecutor.Execute<INoteUserHandler, NoteUser>(h => h.FindByToken(tokenId));
            }
        }

        public void ActivateUser(Guid userId)
        {
            using (var queryExecutor = queryExecutorFactory.Create())
            {
                var user = queryExecutor.Execute<INoteUserHandler, NoteUser>(h => h.Find(userId));
                user.IsActive = true;
                queryExecutor.Execute<INoteUserHandler>(h => h.Update(user));
            }
        }

        public NoteUser Read(string email)
        {
            using (var queryExecutor = queryExecutorFactory.Create())
            {
                return queryExecutor.Execute<INoteUserHandler, NoteUser>(h => h.Read(email));
            }
        }

        public NoteUser ReadByToken(Guid tokenId)
        {
            using (var queryExecutor = queryExecutorFactory.Create())
            {
                var token = queryExecutor.Execute<IUserTokenHandler, UserToken>(h => h.Read(tokenId));
                return queryExecutor.Execute<INoteUserHandler, NoteUser>(h => h.Read(token.UserId));
            }
        }

        public Guid CreateOrGetTokenToUser(NoteUser noteUser)
        {
            using (var queryExecutor = queryExecutorFactory.Create())
            {
                var userToken = queryExecutor.Execute<IUserTokenHandler, UserToken>(h => h.Find(noteUser.Id));
                if (userToken != null)
                    return userToken.Id;

                var token = userTokenFactory.Create(noteUser.Id);
                queryExecutor.Execute<IUserTokenHandler>(h => h.Create(token));
                return token.Id;
            }
        }
    }
}