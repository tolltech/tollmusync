﻿namespace Tolltech.MusyncUI.Implementaitions
{
    public interface IXorCrypto
    {
        byte[] Code(byte[] bytes);
        byte[] Decode(byte[] bytes);

        byte[] CodeNew(byte[] bytes);
        byte[] DecodeNew(byte[] bytes);
    }
}