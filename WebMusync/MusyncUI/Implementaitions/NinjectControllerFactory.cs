﻿using System;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject;
using Tolltech.Musync.Settings;
using Tolltech.ThisCore;
using Tolltech.ThisCore.Sql;
using AuthorizationSettings = Tolltech.MusyncUI.Integration.AuthorizationSettings;

namespace Tolltech.MusyncUI.Implementaitions
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private readonly IKernel ninjectKernel;

        public NinjectControllerFactory()
        {
            ninjectKernel = new StandardKernel();
            Load(ninjectKernel);
            AddBindings();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return (controllerType == null) ? null : (IController) ninjectKernel.Get(controllerType);
        }

        private void AddBindings()
        {
            IoCResolver.Resolve((@interface, implementation) => ninjectKernel.Bind(@interface).To(implementation),
                "Tolltech");

            ninjectKernel.Rebind<IAuthorizationSettings>().To<AuthorizationSettings>();
        }

        private static void Load(IKernel kernel)
        {
            kernel.Load(
                Assembly.GetExecutingAssembly(),
                Assembly.GetAssembly(typeof(IQueryExecutorFactory)));
        }
    }
}