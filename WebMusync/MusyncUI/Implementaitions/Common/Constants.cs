﻿namespace Tolltech.MusyncUI.Implementaitions.Common
{
    public class Constants
    {
        public const string VkUrlCookie = "musync-vk-url-cookie";
        public const string YaLoginCookie = "musync-ya-login-cookie";
    }
}