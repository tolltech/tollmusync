﻿using System;
using JetBrains.Annotations;
using Tolltech.MusyncUI.Models.Sync;

namespace Tolltech.MusyncUI.Implementaitions
{
    public interface IProgressBar
    {
        [CanBeNull]
        ProgressModel GetProgressModel(Guid progressId);

        void UpdateProgressModel([NotNull]ProgressModel progress);
    }
}