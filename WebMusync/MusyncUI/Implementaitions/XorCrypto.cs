﻿using System.Linq;

namespace Tolltech.MusyncUI.Implementaitions
{
    public class XorCrypto : IXorCrypto
    {
        private const byte key = 42;

        public byte[] Code(byte[] bytes)
        {
            return bytes.Select(x => x ^ key).Select(x => (byte)~x).ToArray();
        }

        public byte[] Decode(byte[] bytes)
        {
            return bytes.Select(x => ~x).Select(x => (byte)(x ^ key)).ToArray();
        }

        public byte[] CodeNew(byte[] bytes)
        {
            return bytes.Select(x => x ^ bytes.Length).Select(x => (byte)~x).ToArray();
        }

        public byte[] DecodeNew(byte[] bytes)
        {
            return bytes.Select(x => ~x).Select(x => (byte)(x ^ bytes.Length)).ToArray();
        }
    }
}