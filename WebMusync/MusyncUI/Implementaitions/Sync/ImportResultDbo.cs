﻿using System;
using System.Data.Linq.Mapping;
using Tolltech.Musync.Domain;

namespace Tolltech.MusyncUI.Implementaitions.Sync
{
    [Table(Name = "ImportResults")]
    public class ImportResultDbo
    {
        [Column(Name = "Id", DbType = "uniqueidentifier", IsPrimaryKey = true, CanBeNull = false)]
        public Guid Id { get; set; }

        [Column(Name = "SessionId", DbType = "uniqueidentifier", CanBeNull = false)]
        public Guid SessionId { get; set; }

        [Column(Name = "UserId", DbType = "uniqueidentifier", CanBeNull = false)]
        public Guid UserId { get; set; }

        [Column(Name = "Status", DbType = "int", CanBeNull = false)]
        public ImportStatus Status { get; set; }

        [Column(Name = "Date", DbType = "datetime", CanBeNull = false)]
        public DateTime Date { get; set; }

        [Column(Name = "Title", DbType = "nvarchar", CanBeNull = false)]
        public string Title { get; set; }

        [Column(Name = "Artist", DbType = "nvarchar", CanBeNull = false)]
        public string Artist { get; set; }

        [Column(Name = "NormalizedTitle", DbType = "nvarchar", CanBeNull = false)]
        public string NormalizedTitle { get; set; }

        [Column(Name = "NormalizedArtist", DbType = "nvarchar", CanBeNull = false)]
        public string NormalizedArtist { get; set; }

        [Column(Name = "CandidateTitle", DbType = "nvarchar", CanBeNull = false)]
        public string CandidateTitle { get; set; }

        [Column(Name = "CandidateArtist", DbType = "nvarchar", CanBeNull = false)]
        public string CandidateArtist { get; set; }

        [Column(Name = "Message", DbType = "nvarchar", CanBeNull = false)]
        public string Message { get; set; }
    }
}