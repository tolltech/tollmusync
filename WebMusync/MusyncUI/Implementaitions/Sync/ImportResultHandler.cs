﻿using Tolltech.ThisCore.Sql;

namespace Tolltech.MusyncUI.Implementaitions.Sync
{
    public class ImportResultHandler : IImportResultHandler
    {
        private readonly IDataContext dataContext;

        public ImportResultHandler(IDataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public void Create(ImportResultDbo[] results)
        {
            dataContext.CreateAll(results);
        }
    }
}