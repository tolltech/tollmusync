﻿using System.Threading.Tasks;
using Tolltech.MusyncUI.Models.Sync;

namespace Tolltech.MusyncUI.Implementaitions.Sync
{
    public interface ITrackGetter
    {
        Task<TracksModel> GetTracksAsync(string source);
        string GetAudioUrl(string source);
    }
}