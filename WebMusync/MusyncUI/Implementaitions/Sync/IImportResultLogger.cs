﻿using System;
using Tolltech.Musync.Domain;

namespace Tolltech.MusyncUI.Implementaitions.Sync
{
    public interface IImportResultLogger
    {
        void WriteImportLogs(ImportResult[] results, Guid userId);
    }
}