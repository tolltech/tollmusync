﻿using System;
using System.Linq;
using Tolltech.Musync.Domain;
using Tolltech.ThisCore.Common;
using Tolltech.ThisCore.Sql;

namespace Tolltech.MusyncUI.Implementaitions.Sync
{
    public class ImportResultLogger : IImportResultLogger
    {
        private readonly IQueryExecutorFactory queryExecutorFactory;
        private readonly IGuidGenerator guidGenerator;
        private readonly IDateTimeService dateTimeService;

        public ImportResultLogger(IQueryExecutorFactory queryExecutorFactory, IGuidGenerator guidGenerator,
            IDateTimeService dateTimeService)
        {
            this.queryExecutorFactory = queryExecutorFactory;
            this.guidGenerator = guidGenerator;
            this.dateTimeService = dateTimeService;
        }

        public void WriteImportLogs(ImportResult[] results, Guid userId)
        {
            var now = dateTimeService.Now;
            var sessionId = guidGenerator.Create();
            var dbos = results.Select(x => new ImportResultDbo
            {
                Id = guidGenerator.Create(),
                Date = now,
                UserId = userId,
                SessionId = sessionId,
                Title = x.ImportingTrack.Title,
                Artist = x.ImportingTrack.Artist,
                CandidateArtist = x.Candidate?.Artist ?? string.Empty,
                CandidateTitle = x.Candidate?.Title ?? string.Empty,
                NormalizedArtist = x.NormalizedTrack?.Artist ?? string.Empty,
                NormalizedTitle = x.NormalizedTrack?.Title ?? string.Empty,
                Message = x.Message ?? string.Empty,
                Status = x.ImportStatus
            }).ToArray();

            using (var queryExecutor = queryExecutorFactory.Create())
            {
                queryExecutor.Execute<IImportResultHandler>(x => x.Create(dbos));
            }
        }
    }
}