﻿namespace Tolltech.MusyncUI.Implementaitions.Sync
{
    public interface IImportResultHandler
    {
        void Create(ImportResultDbo[] results);
    }
}