﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Tolltech.Musync.Models;
using Tolltech.MusyncUI.Models.Sync;
using Toltech.YandexApiClient.ApiModels;

namespace Tolltech.MusyncUI.Implementaitions.Extensions
{
    public static class Helpers
    {
        public static bool IsNullOrWhitespace([CanBeNull] this string src)
        {
            return string.IsNullOrWhiteSpace(src);
        }

        [NotNull]
        public static string JoinToString<T>([NotNull] this IEnumerable<T> src, string separator = ", ")
        {
            return string.Join(separator, src);
        }

        [NotNull]
        public static TracksModel ToTracksModel([NotNull] this VkTrack[] tracks)
        {
            return new TracksModel
            {
                Tracks = tracks
                    .Select(x => new TrackModel
                    {
                        Artist = x.Artist,
                        Title = x.Title
                    })
                    .ToList()
            };
        }

        [NotNull]
        public static TracksModel ToTracksModel([NotNull] this Track[] tracks)
        {
            return new TracksModel
            {
                Tracks = tracks
                    .Select(x => new TrackModel
                    {
                        Artist = x.Artists.Select(y => y.Name).JoinToString(),
                        Title = x.Title
                    })
                    .ToList()
            };
        }
    }
}