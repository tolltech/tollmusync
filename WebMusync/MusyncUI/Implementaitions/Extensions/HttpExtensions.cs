﻿using System.Web;
using JetBrains.Annotations;

namespace Tolltech.MusyncUI.Implementaitions.Extensions
{
    public static class HttpExtensions
    {
        public static void SetCookies(this HttpResponseBase response, [NotNull] string name, string val)
        {
            var httpCookie = new HttpCookie(name, val) {HttpOnly = true, Domain = ".musync.ru"};
            response.SetCookie(httpCookie);
        }

        [CanBeNull]
        public static string FindCookies(this HttpRequestBase request, string name)
        {
            return request.Cookies[name]?.Value;
        }
    }
}