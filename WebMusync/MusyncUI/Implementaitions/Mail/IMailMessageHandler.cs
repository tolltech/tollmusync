﻿namespace Tolltech.MusyncUI.Implementaitions.Mail
{
    public interface IMailMessageHandler
    {
        void Create(MailMessage mailMessage);
    }
}