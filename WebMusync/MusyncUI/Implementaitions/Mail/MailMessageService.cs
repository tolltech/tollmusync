﻿using Tolltech.ThisCore.Common;
using Tolltech.ThisCore.Sql;

namespace Tolltech.MusyncUI.Implementaitions.Mail
{
    public class MailMessageService : IMailMessageService
    {
        private readonly IQueryExecutorFactory queryExecutorFactory;
        private readonly IGuidGenerator guidGenerator;
        private readonly IDateTimeService dateTimeService;

        public MailMessageService(IQueryExecutorFactory queryExecutorFactory, IGuidGenerator guidGenerator, IDateTimeService dateTimeService)
        {
            this.queryExecutorFactory = queryExecutorFactory;
            this.guidGenerator = guidGenerator;
            this.dateTimeService = dateTimeService;
        }

        public void Create(string email, string subject, string message)
        {
            using (var queryExecutor = queryExecutorFactory.Create())
            {
                queryExecutor.Execute<IMailMessageHandler>(h => h.Create(new MailMessage
                    {
                        Id = guidGenerator.Create(),
                        CreateDate = dateTimeService.Now,
                        Email = email,
                        Message = message,
                        Subject = subject
                    }));
            }
        }
    }
}