﻿using System;
using System.Data.Linq.Mapping;

namespace Tolltech.MusyncUI.Implementaitions.Mail
{
    [Table(Name = "MailMessages")]
    public class MailMessage
    {
        [Column(Name = "Id", DbType = "uniqueidentifier", IsPrimaryKey = true, CanBeNull = false)]
        public Guid Id { get; set; }

        [Column(Name = "Email", DbType = "nvarchar(1024)", CanBeNull = false)]
        public string Email { get; set; }

        [Column(Name = "Message", DbType = "nvarchar(1024)", CanBeNull = false)]
        public string Message { get; set; }

        [Column(Name = "Subject", DbType = "nvarchar(1024)", CanBeNull = false)]
        public string Subject { get; set; }

        [Column(Name = "CreateDate", DbType = "dateTime", CanBeNull = false)]
        public DateTime CreateDate { get; set; }
    }
}