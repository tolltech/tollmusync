﻿using Tolltech.ThisCore.Sql;

namespace Tolltech.MusyncUI.Implementaitions.Mail
{
    public class MailMessageHandler : IMailMessageHandler
    {
        private readonly IDataContext dataContext;

        public MailMessageHandler(IDataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public void Create(MailMessage mailMessage)
        {
            dataContext.Create(mailMessage);
        }
    }
}