﻿namespace Tolltech.MusyncUI.Implementaitions.Mail
{
    public interface IMailMessageService
    {
        void Create(string email, string subject, string message);
    }
}