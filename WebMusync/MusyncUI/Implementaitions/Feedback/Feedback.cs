﻿using System;
using System.Data.Linq.Mapping;

namespace Tolltech.MusyncUI.Implementaitions.Feedback
{
    [Table(Name = "Feedbacks")]
    public class Feedback
    {

        [Column(Name = "Id", DbType = "uniqueidentifier", IsPrimaryKey = true, CanBeNull = false)]
        public Guid Id { get; set; }

        [Column(Name = "Email", DbType = "nvarchar(1024)", CanBeNull = true)]
        public string Email { get; set; }

        [Column(Name = "Title", DbType = "nvarchar(1024)", CanBeNull = true)]
        public string Title { get; set; }

        [Column(Name = "Text", DbType = "nvarchar(MAX)", CanBeNull = true)]
        public string Text { get; set; }

        [Column(Name = "UserId", DbType = "uniqueidentifier", CanBeNull = true)]
        public Guid? UserId { get; set; }

        [Column(Name = "CreateDate", DbType = "dateTime", CanBeNull = false)]
        public DateTime CreateDate { get; set; }
    }
}