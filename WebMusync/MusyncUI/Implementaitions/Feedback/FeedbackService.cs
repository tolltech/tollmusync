﻿using System;
using Tolltech.ThisCore.Common;
using Tolltech.ThisCore.Sql;

namespace Tolltech.MusyncUI.Implementaitions.Feedback
{
    public class FeedbackService : IFeedbackService
    {
        private readonly IQueryExecutorFactory queryExecutorFactory;
        private readonly IDateTimeService dateTimeService;
        private readonly IGuidGenerator guidGenerator;

        public FeedbackService(IQueryExecutorFactory queryExecutorFactory, IDateTimeService dateTimeService, IGuidGenerator guidGenerator)
        {
            this.queryExecutorFactory = queryExecutorFactory;
            this.dateTimeService = dateTimeService;
            this.guidGenerator = guidGenerator;
        }

        public void Create(string text, string title, string email, Guid? userId)
        {
            using (var queryExecutor = queryExecutorFactory.Create())
            {
                queryExecutor.Execute<IFeedbackHandler>(h => h.Create(new Feedback
                    {
                        Id = guidGenerator.Create(),
                        CreateDate = dateTimeService.Now,
                        Email = email,
                        Text = text,
                        Title = title,
                        UserId = userId
                    }));
            }
        }
    }
}