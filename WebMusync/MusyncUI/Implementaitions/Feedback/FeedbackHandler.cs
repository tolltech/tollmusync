﻿using Tolltech.ThisCore.Sql;

namespace Tolltech.MusyncUI.Implementaitions.Feedback
{
    public class FeedbackHandler : IFeedbackHandler
    {
        private readonly IDataContext dataContext;

        public FeedbackHandler(IDataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public void Create(Feedback feedback)
        {
            dataContext.Create(feedback);
        }
    }
}