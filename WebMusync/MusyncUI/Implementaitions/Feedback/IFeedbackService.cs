﻿using System;

namespace Tolltech.MusyncUI.Implementaitions.Feedback
{
    public interface IFeedbackService
    {
        void Create(string text, string title, string email, Guid? userId);
    }
}