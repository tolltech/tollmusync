﻿namespace Tolltech.MusyncUI.Implementaitions.Feedback
{
    public interface IFeedbackHandler
    {
        void Create(Feedback feedback);
    }
}