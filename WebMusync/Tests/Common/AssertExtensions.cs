﻿using NUnit.Framework;
using Tolltech.ThisCore;

namespace Tolltech.Tests.Common
{
    public static class AssertJson
    {
        private static IJsonSerializer jsonSerializer = null;

        private static IJsonSerializer serializer => jsonSerializer ?? (jsonSerializer = new Serializer());

        public static void AreEqual<T>(T expected, T actual)
        {
            var expectedStr = serializer.SerializeToString(expected);
            var actualStr = serializer.SerializeToString(actual);
            Assert.AreEqual(expectedStr, actualStr);
        }
    }
}