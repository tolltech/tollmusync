using System.Reflection;
using NUnit.Framework;
using Ninject;
using Tolltech.ThisCore;
using Tolltech.ThisCore.Sql;
using Tolltech.ThisCore.Tolltech.Core.Configuration;
using Tolltech.MusyncUI.Implementaitions.Users;

namespace Tolltech.Tests.Common
{
    [TestFixture]
    public class TestBase
    {
        protected readonly StandardKernel standardKernel;
        protected readonly IQueryExecutorFactory queryExecutorFactory;
        protected IDataContext dataContext(string connectionStringKey = ConfigKeys.DbConnectionStringKey)
        {
            return new DataContextImpl(configuration.Get(connectionStringKey));
        }

        ////��� ����� ��������� ������
        //private Topology topology;
        private readonly IConfiguration configuration;
        //private readonly INoteUserHandler noteUserHandler;

        public TestBase()
        {
            standardKernel = new StandardKernel();
            Load(standardKernel);
            standardKernel.Load(new ConfigurationModule());

            standardKernel.Rebind<IConfiguration>().To<Configuration.Configuration>();
            configuration = standardKernel.Get<IConfiguration>();
            //            dataContext = standardKernel.Get<IDataContext>();
            queryExecutorFactory = standardKernel.Get<IQueryExecutorFactory>();
        }

        private static void Load(IKernel kernel)
        {
            kernel.Load(
                   Assembly.GetExecutingAssembly(),
                   Assembly.GetAssembly(typeof(IQueryExecutorFactory)),
                   Assembly.GetAssembly(typeof(INoteUserHandler)));
        }

        protected virtual void SetUp()
        {

        }

        [SetUp]
        public void Init()
        {
            SetUp();
        }
    }
}