﻿using System.Linq;
using Tolltech.ThisCore.Sql;

namespace Tolltech.Tests.Common
{
    public class DBHelper
    {
        public DBHelper()
        {
        }

        public static void Create<T>(IDataContext dataContext, T entity) where T : class
        {
            dataContext.Create(entity);
        }

        public static void CreateAll<T>(IDataContext dataContext, T[] entities) where T : class
        {
            dataContext.CreateAll(entities);
        }

        public static T[] SelectAll<T>(IDataContext dataContext) where T : class
        {
            return dataContext.GetTable<T>().ToArray();            
        }

        public static void Truncate<T>(IDataContext dataContext) where T : class
        {
            var table = dataContext.GetTable<T>();
            dataContext.DeleteAll(table);
        }
    }
}