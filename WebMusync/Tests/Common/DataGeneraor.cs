﻿using System;
using System.Text;

namespace Tolltech.Tests.Common
{
    public static class DataGeneraor
    {
        public static string GetString(int length)
        {
            var sb = new StringBuilder();
            while (sb.Length < length)
            {
                sb.Append(Guid.NewGuid().ToString());
            }
            return sb.ToString(0, length);
        }
    }
}