﻿using System.Configuration;
using Tolltech.ThisCore;

namespace Tolltech.Tests.Configuration
{
    public class TestConfigSection : ConfigurationSection
    {
        [ConfigurationProperty(ConfigKeys.DbConnectionStringKey, DefaultValue = "", IsKey = false, IsRequired = true)]
        public string DBConnectionString { get; set; }

        [ConfigurationProperty(ConfigKeys.UIDbConnectionStringKey, DefaultValue = "", IsKey = false, IsRequired = true)]
        public string UiDBConnectionString { get; set; }

        [ConfigurationProperty(ConfigKeys.FriendsDbConnectionStringKey, DefaultValue = "", IsKey = false, IsRequired = true)]
        public string FriendsDBConnectionString { get; set; } 
    }
}