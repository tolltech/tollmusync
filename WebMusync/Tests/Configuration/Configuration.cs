﻿using System.Configuration;
using Tolltech.ThisCore;

namespace Tolltech.Tests.Configuration
{
    public class Configuration : IConfiguration
    {
        public string Get(string key)
        {
            return ((TestConfigSection)ConfigurationManager.GetSection(ConfigKeys.ServiceSectionNameKey)).ElementInformation.Properties[key].Value.ToString();
        }
    }
}