﻿using System;
using NUnit.Framework;
using Tolltech.ThisCore;
using Tolltech.MusyncUI.Implementaitions.Users;
using Tolltech.Tests.Common;
using System.Linq;

namespace Tolltech.Tests.Tests.Notes.UI
{
    public class NoteUserHandlerTest : TestBase
    {
        [Test]
        public void TestCreate()
        {
            var user = new NoteUser
                {
                    Id = Guid.NewGuid(),
                    Email = DataGeneraor.GetString(12),
                    CreateDate = new DateTime(2010, 10, 10),
                    IsActive = true,
                    TokenId = Guid.NewGuid()
                };
            using (var queryExecutor = queryExecutorFactory.Create(ConfigKeys.UIDbConnectionStringKey))
            {
                queryExecutor.Execute<INoteUserHandler>(h => h.Create(user));
            }

            var noteUsers = DBHelper.SelectAll<NoteUser>(dataContext(ConfigKeys.UIDbConnectionStringKey)).Where(x => x.Id == user.Id).ToArray();
            Assert.AreEqual(1, noteUsers.Length);
            AssertJson.AreEqual(user, noteUsers[0]);
        }

        [Test]
        public void TestFindUser()
        {
            var email = DataGeneraor.GetString(10);
            var noteUser = new NoteUser
                {
                    Email = email,
                    Id = Guid.NewGuid(),
                    CreateDate = new DateTime(2010, 10, 10),
                    TokenId = Guid.NewGuid()
                };
            DBHelper.Create(dataContext(ConfigKeys.UIDbConnectionStringKey), noteUser);

            using (var queryExecutor = queryExecutorFactory.Create(ConfigKeys.UIDbConnectionStringKey))
            {
                var user = queryExecutor.Execute<INoteUserHandler, NoteUser>(h => h.Find(DataGeneraor.GetString(4)));
                Assert.IsNull(user);

                user = queryExecutor.Execute<INoteUserHandler, NoteUser>(h => h.Find(email));
                AssertJson.AreEqual(noteUser, user);
            }
        }


        [Test]
        public void TestUpdate()
        {
            var user = new NoteUser
            {
                Id = Guid.NewGuid(),
                Email = DataGeneraor.GetString(12),
                CreateDate = new DateTime(2010, 10, 10),
                IsActive = true,
                TokenId = Guid.NewGuid()
            };

            var tokenId = Guid.NewGuid();
            DBHelper.Create(dataContext(ConfigKeys.UIDbConnectionStringKey), user);
            using (var queryExecutor = queryExecutorFactory.Create(ConfigKeys.UIDbConnectionStringKey))
            {
                var noteUser = queryExecutor.Execute<INoteUserHandler, NoteUser>(h => h.Find(user.Id));
                noteUser.TokenId = tokenId;
                queryExecutor.Execute<INoteUserHandler>(h => h.Update(noteUser));
            }

            var noteUsers = DBHelper.SelectAll<NoteUser>(dataContext(ConfigKeys.UIDbConnectionStringKey)).Where(x => x.Id == user.Id).ToArray();
            Assert.AreEqual(1, noteUsers.Length);
            Assert.AreNotEqual(user.TokenId, tokenId);
            Assert.AreEqual(tokenId, noteUsers[0].TokenId);
        }
    }
}