﻿using System;
using System.Linq;
using NUnit.Framework;
using Tolltech.ThisCore;
using Tolltech.MusyncUI.Implementaitions.Feedback;
using Tolltech.Tests.Common;

namespace Tolltech.Tests.Tests.Notes.UI
{
    public class FeedbackHandlerTest : TestBase
    {
        [Test]
        public void TestCreate()
        {
            var feedback = new Feedback
            {
                Id = Guid.NewGuid(),
                Email = DataGeneraor.GetString(12),
                CreateDate = new DateTime(2010, 10, 10),
                Text = "text",
                Title = "title",
                UserId = Guid.NewGuid()
            };
            using (var queryExecutor = queryExecutorFactory.Create(ConfigKeys.UIDbConnectionStringKey))
            {
                queryExecutor.Execute<IFeedbackHandler>(h => h.Create(feedback));
            }

            var feedbacks = DBHelper.SelectAll<Feedback>(dataContext(ConfigKeys.UIDbConnectionStringKey)).Where(x => x.Id == feedback.Id).ToArray();
            Assert.AreEqual(1, feedbacks.Length);
            AssertJson.AreEqual(feedback, feedbacks[0]);
        }
    }
}