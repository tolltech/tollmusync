﻿using System.Text;
using NUnit.Framework;
using Ninject;
using Tolltech.MusyncUI.Implementaitions;
using Tolltech.Tests.Common;

namespace Tolltech.Tests.Tests.Notes.UI
{
    public class XorCryptoTest : TestBase
    {
        private IXorCrypto xorCrypto;

        protected override void SetUp()
        {
            base.SetUp();

            xorCrypto = standardKernel.Get<IXorCrypto>();
        }

        [Test]
        public void TestCodeDecode()
        {
            var text = "message";
            var bytes = Encoding.UTF8.GetBytes(text);
            var codedBytes = xorCrypto.Code(bytes);
            var decodedBytes = xorCrypto.Decode(codedBytes);
            var actual = Encoding.UTF8.GetString(decodedBytes);
            Assert.AreEqual(text, actual);
        }
    }
}