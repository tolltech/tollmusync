﻿using System;
using System.Linq;
using NUnit.Framework;
using Tolltech.ThisCore;
using Tolltech.MusyncUI.Implementaitions.Mail;
using Tolltech.Tests.Common;

namespace Tolltech.Tests.Tests.Notes.UI
{
    public class MailMessageHandlerTest : TestBase
    {
        [Test]
        public void TestCreate()
        {
            var mailMessage = new MailMessage
            {
                Id = Guid.NewGuid(),
                Email = DataGeneraor.GetString(12),
                CreateDate = new DateTime(2010, 10, 10),
                Message = "text",
                Subject = "title",
            };
            using (var queryExecutor = queryExecutorFactory.Create(ConfigKeys.UIDbConnectionStringKey))
            {
                queryExecutor.Execute<IMailMessageHandler>(h => h.Create(mailMessage));
            }

            var messages = DBHelper.SelectAll<MailMessage>(dataContext(ConfigKeys.UIDbConnectionStringKey)).Where(x => x.Id == mailMessage.Id).ToArray();
            Assert.AreEqual(1, messages.Length);
            AssertJson.AreEqual(mailMessage, messages[0]);
        }
    }
}