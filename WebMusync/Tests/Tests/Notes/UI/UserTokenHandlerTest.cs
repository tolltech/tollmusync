﻿using System;
using System.Linq;
using NUnit.Framework;
using Tolltech.ThisCore;
using Tolltech.MusyncUI.Implementaitions.Users;
using Tolltech.Tests.Common;

namespace Tolltech.Tests.Tests.Notes.UI
{
    public class UserTokenHandlerTest : TestBase
    {
        [Test]
        public void TestCreate()
        {
            var userToken = new UserToken
            {
                Id = Guid.NewGuid(),
                CreateDate = new DateTime(2010, 10, 10),
                UserId = Guid.NewGuid()
            };
            using (var queryExecutor = queryExecutorFactory.Create(ConfigKeys.UIDbConnectionStringKey))
            {
                queryExecutor.Execute<IUserTokenHandler>(h => h.Create(userToken));
            }

            var feedbacks = DBHelper.SelectAll<UserToken>(dataContext(ConfigKeys.UIDbConnectionStringKey)).Where(x => x.Id == userToken.Id).ToArray();
            Assert.AreEqual(1, feedbacks.Length);
            AssertJson.AreEqual(userToken, feedbacks[0]);
        }
    }
}