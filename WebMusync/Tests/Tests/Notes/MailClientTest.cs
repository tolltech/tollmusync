﻿using System;
using NUnit.Framework;
using Tolltech.ThisCore.Mail;
using Tolltech.Tests.Common;
using Ninject;

namespace Tolltech.Tests.Tests.Notes
{
    public class MailClientTest : TestBase
    {
        private IMailClient mailClient;

        protected override void SetUp()
        {
            base.SetUp();

            mailClient = standardKernel.Get<IMailClient>();
        }

        [Test]
        public void TestSend()
        {
            mailClient.Send("alexandrovpe@gmail.com", Guid.NewGuid().ToString(), Guid.NewGuid().ToString());
        }
    }
}